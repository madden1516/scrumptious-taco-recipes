from django.shortcuts import redirect, render, get_object_or_404
from recipes.models import Recipe
from recipes.forms import RecipeForm

# Create your views here.
def show_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    context = {
        "recipe": recipe,
    }
    return render(request, "recipes/detail.html", context)

def recipe_list(request):
    recipes = Recipe.objects.all()
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipes/list.html", context)

def create_recipe(request):
    if request.method == "POST":
        # We should use the form to validate the values
        #   and save them to the database
        form = RecipeForm(request.POST)
        if form.is_valid():
            form.save()
            # If all goes well, we can redirect the browser
            #    to another page and leave the function
            return redirect("recipe_list")
    else:
    # Create an instance of the Django model form class
        form = RecipeForm()

         # Put the form in the context
    context = {
        "form": form,
    }
         # Render the HTML template with the form
    return render(request, "recipes/create.html", context)

def edit_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    if request.method == "POST":
        form = RecipeForm(request.POST, instance = recipe)
        if form.is_valid():
            form.save()
            return redirect("show_recipe", id=id)
    else:
        form = RecipeForm()

    
    context = {
        "recipe_object": recipe,
        "form": form,
        }
    return render(request, "recipes/edit.html", context)
